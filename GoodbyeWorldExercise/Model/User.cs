﻿using System;
namespace GoodbyeWorldExercise
{
    public class User
    {
        public static User user;

        public static User getInstance
        {
            get
            {
                if (user == null)
                    user = new User();

                return user;
            }
        }

        public string Name
        {
            get; set;

        }
        public string Email
        {
            get; set;

        }
        public string Password
        {
            get; set;

        }
    }
}
