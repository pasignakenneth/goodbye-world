﻿using System;
namespace GoodbyeWorldExercise
{
    public class Task
    {
        public string Title
        {
            get; set;
        }
        public string Address
        {
            get; set;
        }
        public string Price
        {
            get; set;
        }
        public string Payment
        {
            get; set;
        }
    }
}
