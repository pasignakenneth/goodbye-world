﻿using System;
using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
	public class CancelButtonTrigger : TriggerAction<Button>
    {
        protected override void Invoke(Button button)
        {
            if (button.Text == "CANCEL")
            {
                button.IsEnabled = false;
                button.IsVisible = false;
            }
            else if(button.Text=="ABORT")
            {
                button.IsEnabled = false;
                button.IsVisible = false;
                button.Text = "CANCEL";
            }
        }
    }
} 