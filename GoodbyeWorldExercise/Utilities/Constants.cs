﻿using System;
using System.Collections.ObjectModel;

namespace GoodbyeWorldExercise.Utilities
{
    public class Constants
    {
        public static ObservableCollection<User> newUser = new ObservableCollection<User>();
        public static ObservableCollection<Task> newTask = new ObservableCollection<Task>();
        public static ObservableCollection<Task> newJob = new ObservableCollection<Task>();
        public static ObservableCollection<Task> newMyTask = new ObservableCollection<Task>();
    }
}
