﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class LandingPage : MasterDetailPage
    {
        public LandingPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            TaskList();
        }

        void TaskMenu_Clicked()
        {
            Detail = new NavigationPage(new TaskPage());
            IsPresented = false;

        }
        void JobMenu_Clicked()
        {
            Detail = new NavigationPage(new JobPage());
            IsPresented = false;
        }
        void MyTaskMenu_Clicked()
        {
            Detail = new NavigationPage(new MyTaskPage());
            IsPresented = false;
        }
        void LogoutMenu_Clicked()
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());
            Application.Current.Properties.Remove("user_email");
            Application.Current.SavePropertiesAsync();
        }
        void TaskList()
        {
            Utilities.Constants.newTask.Add(new Task { Title = "Clean House", Address = "Cebu", Price = "20", Payment = "CASH" });
            Utilities.Constants.newTask.Add(new Task { Title = "Clean Car", Address = "Manila", Price = "50", Payment = "CREDIT" });
        }


    }
}
