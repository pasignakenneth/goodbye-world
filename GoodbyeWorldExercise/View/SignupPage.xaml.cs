﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class SignupPage : ContentPage
    {
        User user = User.getInstance;

        public SignupPage()
        {
            InitializeComponent();
        }

        void SignupButton_Clicked(object sender, EventArgs eventArgs)
        {
            if (!String.IsNullOrEmpty(NameEntry.Text) && !String.IsNullOrEmpty(EmailEntry.Text) && !String.IsNullOrEmpty(PasswordEntry.Text) && !String.IsNullOrEmpty(ConfirmPasswordEntry.Text))
            {
                Utilities.Constants.newUser.Add(new User() { Name = NameEntry.Text, Email = EmailEntry.Text, Password = PasswordEntry.Text });
                Application.Current.Properties.Add("user_email", EmailEntry.Text);
                Application.Current.SavePropertiesAsync();
                Application.Current.MainPage = new NavigationPage(new LandingPage());
            }
            string req = "Required";
            NameEntry.Placeholder = req;
            NameEntry.PlaceholderColor = Color.Red;
            EmailEntry.Placeholder = req;
            EmailEntry.PlaceholderColor = Color.Red;
            PasswordEntry.Placeholder = req;
            PasswordEntry.PlaceholderColor = Color.Red;
            ConfirmPasswordEntry.Placeholder = req;
            ConfirmPasswordEntry.PlaceholderColor = Color.Red;

        }
        void SigninButton_Clicked(object sender, EventArgs eventArgs)
        {
            Navigation.PopAsync();
        }
    }
}
