﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class MyTaskSelectedPage : ContentPage
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create("HeaderText", typeof(string), typeof(MyTaskSelectedPage), null);
        public static readonly BindableProperty EditIconProperty = BindableProperty.Create("EditIcon", typeof(string), typeof(MyTaskSelectedPage), null);
        public static readonly BindableProperty EditIconCommandProperty = BindableProperty.Create("EditIcon_Command", typeof(ICommand), typeof(MyTaskSelectedPage), null);
        public static readonly BindableProperty AddIconProperty = BindableProperty.Create("AddIcon", typeof(string), typeof(MyTaskSelectedPage), null);
        public static readonly BindableProperty AddIconCommandProperty = BindableProperty.Create("AddIcon_Command", typeof(ICommand), typeof(MyTaskSelectedPage), null);
        public static readonly BindableProperty BackButtonProperty = BindableProperty.Create("BackButton", typeof(string), typeof(MyTaskSelectedPage), null);
        public static readonly BindableProperty BackButtonCommandProperty = BindableProperty.Create("BackButton_Command", typeof(ICommand), typeof(MyTaskPage), null);

        Task task = new Task();

        public MyTaskSelectedPage(Task task)
        {
            BindingContext = task;
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.task = task;
            this.HeaderText = task.Title;
            this.BackButton = "back";
            this.EditIcon = "edit";
            this.EditIcon_Command = new Command((obj) => Navigation.PushAsync(new EditMyTaskPage(task)));
            this.AddIcon = "delete";
            this.AddIcon_Command = new Command((obj) => MyTask_Removed());
            this.BackButton_Command = new Command((obj) => Navigation.PopAsync());

        }
        public string BackButton
        {
            get
            {
                return (string)GetValue(BackButtonProperty);
            }
            set
            {
                SetValue(BackButtonProperty, value);
            }

        }
        public ICommand BackButton_Command
        {
            get
            {
                return (ICommand)GetValue(BackButtonCommandProperty);
            }
            set
            {
                SetValue(BackButtonCommandProperty, value);
            }
        }
        public string HeaderText
        {
            get
            {
                return (string)GetValue(HeaderTextProperty);
            }
            set
            {
                SetValue(HeaderTextProperty, value);
            }
        }
        public string EditIcon
        {
            get
            {
                return (string)GetValue(EditIconProperty);
            }
            set
            {
                SetValue(EditIconProperty, value);
            }
        }
        public ICommand EditIcon_Command
        {
            get
            {
                return (ICommand)GetValue(EditIconCommandProperty);

            }
            set
            {
                SetValue(EditIconCommandProperty, value);
            }
        }
        public string AddIcon
        {
            get
            {
                return (string)GetValue(AddIconProperty);
            }
            set
            {
                SetValue(AddIconProperty, value);
            }
        }
        public ICommand AddIcon_Command
        {
            get
            {
                return (ICommand)GetValue(AddIconCommandProperty);
            }
            set
            {
                SetValue(AddIconCommandProperty, value);
            }
        }
        async void MyTask_Removed()
        {
            var response = await DisplayAlert("Delete", "Are you sure?", "Okay", "Cancel");

            if (response)
            {
                Utilities.Constants.newMyTask.Remove(task);
                await Navigation.PopAsync();
            }
        }
        void CancelButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();

        }
        void DoneButton_Clicked(object sender, EventArgs e)
        {
            Button button = sender as Button;

            if(button.Text == "DONE")
            {
                button.IsVisible = false;
                button.IsEnabled = false;
                cancel.IsVisible = false;
                cancel.IsEnabled = false;
                rate.IsVisible = true;
                rate.IsEnabled = true;

            }
        }
        void RateButton_Clicked(object sender, EventArgs e)
        {
            Utilities.Constants.newMyTask.Remove(task);
            Navigation.PopAsync();

        }
    }
}
