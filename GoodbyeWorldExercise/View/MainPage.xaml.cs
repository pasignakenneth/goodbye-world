﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class MainPage : ContentPage
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create("HeaderText", typeof(string), typeof(MainPage), "SIGN UP");

        public MainPage()
        {
            InitializeComponent();
             

        }
        public string HeaderText
        {
            get
            {
                return (string)GetValue(HeaderTextProperty);
            }
        }
        void SignupButton_Clicked(object sender, EventArgs eventArgs)
        {
            Navigation.PushAsync(new SignupPage());

        }
        void SigninButton_Clicked(object sender, EventArgs eventArgs)
        {
            string emailtext = EmailSigninEntry.Text.Trim();

            try
            {
                if (!String.IsNullOrEmpty(EmailSigninEntry.Text) && !String.IsNullOrEmpty(PasswordSigninEntry.Text))
                {
                    if (Utilities.Constants.newUser.Any(p => p.Email == emailtext))
                    {
                        Application.Current.Properties.Add("user_email", emailtext);
                        Application.Current.SavePropertiesAsync();
                        Application.Current.MainPage = new NavigationPage(new LandingPage());

                    }
                }
                string req = "Required";
                EmailSigninEntry.Placeholder = req;
                EmailSigninEntry.PlaceholderColor = Color.Red;
                PasswordSigninEntry.Placeholder = req;
                PasswordSigninEntry.PlaceholderColor = Color.Red;
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }
    }
}
