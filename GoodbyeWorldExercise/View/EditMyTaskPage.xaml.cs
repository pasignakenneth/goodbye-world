﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class EditMyTaskPage : ContentPage
    {
        Task task = new Task();
        string payment;
        public EditMyTaskPage(Task task)
        {
            BindingContext = task;
            InitializeComponent();
            payment = task.Payment;
            this.task = task;

            if (task.Payment == "CASH")
                CashButton.IsEnabled = false;
            else if (task.Payment == "CREDIT")
                CreditButton.IsEnabled = false;

        }
        void CashButton_Clicked(object sender, EventArgs eventArgs)
        {
            CashButton.IsEnabled = false;
            CreditButton.IsEnabled = true;
            payment = CashButton.Text;
        }
        void CreditButton_Clicked(object sender, EventArgs eventargs)
        {
            CreditButton.IsEnabled = false;
            CashButton.IsEnabled = true;
            payment = CreditButton.Text;
        }
        void EditButton_Clicked(object sender, EventArgs eventArgs)
        {
            var item = Utilities.Constants.newMyTask.FirstOrDefault(i => i.Title == task.Title);
            if(item!=null)
            {
                if (!String.IsNullOrEmpty(TitleEntry.Text) && !String.IsNullOrEmpty(AddressEntry.Text) && !String.IsNullOrEmpty(PriceEntry.Text) && !String.IsNullOrEmpty(payment))
                {
                    item.Title = TitleEntry.Text;
                    item.Address = AddressEntry.Text;
                    item.Price = PriceEntry.Text;
                    item.Payment = payment;

                    Navigation.PopToRootAsync();
                }
                string req = "Required";
                PriceEntry.Placeholder = req;
                PriceEntry.PlaceholderColor = Color.Red;
                TitleEntry.Placeholder = req;
                TitleEntry.PlaceholderColor = Color.Red;
                AddressEntry.Placeholder = req;
                AddressEntry.PlaceholderColor = Color.Red;
            }

        }
    }
}
