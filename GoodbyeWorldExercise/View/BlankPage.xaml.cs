﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class BlankPage : ContentPage
    {
        public static readonly BindableProperty BackButtonProperty = BindableProperty.Create("BackButton", typeof(string), typeof(JobPage), null);
        public static readonly BindableProperty BackButtonCommandProperty = BindableProperty.Create("BackButton_Command", typeof(ICommand), typeof(JobPage), null);

        public BlankPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BackButton = "burger";
            this.BackButton_Command = new Command((obj) => ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).IsPresented = true);
        }
        public string BackButton
        {
            get
            {
                return (string)GetValue(BackButtonProperty);
            }
            set
            {
                SetValue(BackButtonProperty, value);
            }

        }
        public ICommand BackButton_Command
        {
            get
            {
                return (ICommand)GetValue(BackButtonCommandProperty);
            }
            set
            {
                SetValue(BackButtonCommandProperty, value);
            }
        }
        public string HeaderText
        {
            get
            {
                return "LANDING";
            }
        }
    }
}
