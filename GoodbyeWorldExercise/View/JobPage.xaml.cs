﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class JobPage : ContentPage
    {
        public static readonly BindableProperty BackButtonProperty = BindableProperty.Create("BackButton", typeof(string), typeof(JobPage), null);
        public static readonly BindableProperty BackButtonCommandProperty = BindableProperty.Create("BackButton_Command", typeof(ICommand), typeof(JobPage), null);

        public JobPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            jobList.ItemsSource = Utilities.Constants.newJob;
            this.BackButton = "burger";
            this.BackButton_Command = new Command((obj) => ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).IsPresented = true);

        }
        public string BackButton
        {
            get
            {
                return (string)GetValue(BackButtonProperty);
            }
            set
            {
                SetValue(BackButtonProperty, value);
            }

        }
        public ICommand BackButton_Command
        {
            get
            {
                return (ICommand)GetValue(BackButtonCommandProperty);
            }
            set
            {
                SetValue(BackButtonCommandProperty, value);
            }
        }
        public string HeaderText
        {
            get
            {
                return "Jobs";
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            jobList.ItemsSource = null;
            jobList.ItemsSource = Utilities.Constants.newJob;
        }
        void JobList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Task job = (Task)jobList.SelectedItem;
            Navigation.PushAsync(new JobSelectedPage(job));
        }
    }
}
