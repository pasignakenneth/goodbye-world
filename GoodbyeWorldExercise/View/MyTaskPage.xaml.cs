﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class MyTaskPage : ContentPage
    {
        public static readonly BindableProperty AddIconProperty = BindableProperty.Create("AddIcon", typeof(string), typeof(MyTaskPage), null);
        public static readonly BindableProperty AddIconCommandProperty = BindableProperty.Create("AddIcon_Command", typeof(ICommand), typeof(MyTaskPage), null);
        public static readonly BindableProperty BackButtonProperty = BindableProperty.Create("BackButton", typeof(string), typeof(MyTaskPage),null);
        public static readonly BindableProperty BackButtonCommandProperty = BindableProperty.Create("BackButton_Command", typeof(ICommand), typeof(MyTaskPage), null);

        public MyTaskPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            myTaskList.ItemsSource = Utilities.Constants.newMyTask;
            this.AddIcon_Command = new Command((obj) => Navigation.PushAsync(new NewMyTaskPage()));
            this.BackButton = "burger";
            this.BackButton_Command = new Command((obj) => ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).IsPresented =true );
        }
        public string BackButton
        {
            get
            {
                return (string)GetValue(BackButtonProperty);
            }
            set
            {
                SetValue(BackButtonProperty, value);
            }

        }
        public ICommand BackButton_Command
        {
            get
            {
                return (ICommand)GetValue(BackButtonCommandProperty);
            }
            set
            {
                SetValue(BackButtonCommandProperty, value);
            }
        }
        public string HeaderText
        {
            get
            {
                return "My Tasks";
            }
        }
        public string AddIcon
        {
            get
            {
                return "add";
            }

        }
        public ICommand AddIcon_Command
        {
            get
            {
                return (ICommand)GetValue(AddIconCommandProperty);
            }
            set
            {
                SetValue(AddIconCommandProperty, value);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            myTaskList.ItemsSource = null;
            myTaskList.ItemsSource = Utilities.Constants.newMyTask;

        }
        void MyTaskList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Task task = (Task)myTaskList.SelectedItem;
            Navigation.PushAsync(new MyTaskSelectedPage(task));

        }
    }
}
