﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class TaskSelectedPage : ContentPage
    {
        Task task = new Task();

        public TaskSelectedPage(Task task)
        {
            BindingContext = task;
            InitializeComponent();

            this.task = task;
        }
        void AcceptButton_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = sender as Button;
            Utilities.Constants.newJob.Add(new Task { Title = task.Title, Address = task.Address, Payment = task.Payment, Price = task.Price });
            Utilities.Constants.newTask.Remove(task);
            button.IsVisible = false;
            button.IsEnabled = false;

            Navigation.PushAsync(new JobPage());
        }
        //void AcceptRateButton_Clicked(object sender, EventArgs eventArgs)
        //{
        //    Button button = sender as Button;

        //    if (button.Text == "ACCEPT")
        //    {
        //        Utilities.Constants.newJob.Add(new Task { Title = task.Title, Address = task.Address, Payment = task.Payment, Price = task.Price });
        //        Utilities.Constants.newTask.Remove(task);
        //    }
        //    else if(button.Text == "RATE")
        //    {
        //        Utilities.Constants.newJob.Remove(task);
        //        Navigation.PopAsync();
        //    }
        //}
        //void CancelAbortButton_Clicked(object sender, EventArgs eventArgs)
        //{
        //    Button button = sender as Button;

        //    Utilities.Constants.newTask.Add(task);

        //    if (Utilities.Constants.newJob.Any(p => p.Title == task.Title))
        //        Utilities.Constants.newJob.Remove(task);


        //    if (button.Text == "CANCEL")
        //    {
        //        button.IsEnabled = false;
        //        button.IsVisible = false;

        //    }
        //    if (button.Text == "ABORT")
        //    {
        //        button.IsEnabled = false;
        //        button.IsVisible = false;
        //        button.Text = "CANCEL";

        //    }
        //}
    }
}
