﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class JobSelectedPage : ContentPage
    {
        Task task = new Task();

        public JobSelectedPage(Task task)
        {
            BindingContext = task;
            InitializeComponent();

            this.task = task;

        }
        void StartButton_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = sender as Button;

            if (button.Text == "START")
            {
                button.Text = "DONE";
            }
            else if (button.Text == "DONE")
            {
                button.IsVisible = false;
                button.IsEnabled = false;
                button.Text = "START";
            }

        }
        void CancelButton_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = sender as Button;
            if (button.Text == "CANCEL")
            {
                Utilities.Constants.newTask.Add(new Task { Title = task.Title, Address = task.Address, Payment = task.Payment, Price = task.Price });
                Utilities.Constants.newJob.Remove(task);
                Navigation.PopAsync();
            }
            else if(button.Text == "ABORT")
            {
                button.Text = "CANCEL";
            }

        }
        void RateButton_Clicked(object sender, EventArgs eventArgs)
        {
            rate.IsVisible = false;
            rate.IsEnabled = false;
            Utilities.Constants.newJob.Remove(task);
            Navigation.PopAsync();
        }
    }
}
