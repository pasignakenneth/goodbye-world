﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace GoodbyeWorldExercise
{
    public partial class NewMyTaskPage : ContentPage
    {
        string payment;

        public NewMyTaskPage()
        {
            InitializeComponent();
        }
        void CashButton_Clicked(object sender, EventArgs eventArgs)
        {
            CashButton.IsEnabled = false;
            CreditButton.IsEnabled = true;
            payment = CashButton.Text;

        }
        void CreditButton_Clicked(object sender, EventArgs eventArgs)
        {
            CreditButton.IsEnabled = false;
            CashButton.IsEnabled = true;
            payment = CashButton.Text;

        }
        void SaveButton_Clicked(object sender, EventArgs eventArgs)
        {
            if (!String.IsNullOrEmpty(TitleEntry.Text) && !String.IsNullOrEmpty(AddressEntry.Text) && !String.IsNullOrEmpty(PriceEntry.Text) && !String.IsNullOrEmpty(payment))
            {
                Utilities.Constants.newMyTask.Add(new Task() { Title = TitleEntry.Text, Address = AddressEntry.Text, Price = PriceEntry.Text, Payment = payment });
                Navigation.PopAsync();
            }

            string req = "Required";
            PriceEntry.Placeholder = req;
            PriceEntry.PlaceholderColor = Color.Red;
            TitleEntry.Placeholder = req;
            TitleEntry.PlaceholderColor = Color.Red;
            AddressEntry.Placeholder = req;
            AddressEntry.PlaceholderColor = Color.Red;
        }
    }
}
