using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace GoodbyeWorldExercise
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

        }

        protected override void OnStart()
        {

            if(Application.Current.Properties.ContainsKey("user_email")==true)
            {
                Application.Current.MainPage = new NavigationPage(new LandingPage());
            }
            else
            {
                MainPage = new NavigationPage(new MainPage()); 
            }

        }
        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
